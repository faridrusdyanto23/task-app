# task-app



## Getting started

- create database mysql with name "task_app"
- import database from "./db/task_app.sql"
- "npm install" in the project root directory to install the required libraries
- run project "npm run start"

### Endpoint

- (GET) http://localhost:3001/tasks
- (GET) http://localhost:3001/tasks/{id}
- (POST) http://localhost:3001/task
- (PATCH) http://localhost:3001/tasks/{id}
- (DELETE) http://localhost:3001/tasks/{id}
