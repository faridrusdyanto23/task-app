'use strict';

const knex = require('knex')({
  client: 'mysql2',
  connection: {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'task_app'
  }
});

module.exports = { knex }
