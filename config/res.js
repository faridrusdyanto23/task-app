'use strict';

exports.ok = async (res, status, message, values) => {
  const data = {
    'success': status,
    'message': message,
    'data': values,
  }
  await res.json(data);
  await res.end();
}
