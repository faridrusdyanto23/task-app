'use strict';
const response = require("../config/res");
const model = require("../models/index");

const getData = async (req, res) => {
  try {
    const data = await model.getAll();
    const newData = data.map(item => {
      return {
        ...item,
        completed: item.completed === 1 ? true : false
      }
    });
    await response.ok(res, true, "Data tersedia", newData);

  } catch (err) {
    console.error(err);
  }
}

const getDataById = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await model.getById(id);
    data.completed = data.completed === 1 ? true : false;
    await response.ok(res, true, "Data tersedia", data);

  } catch (err) {
    console.error(err);
  }
}

const addData = async (req, res) => {
  try {
    const { title, description } = req.body;
    await model.addData({ title, description })
    await response.ok(res, true, "Data berhasil disimpan")
    
  } catch (err) {
    console.error(err);
  }
}

const updateData = async (req, res) => {
  try {
    const id = req.params.id;
    const { title, description, completed } = req.body;
    const updates = {};
    if (title) {
      updates.title = title;
    }
    if (description) {
      updates.description = description;
    }
    if (completed === true || completed === false) {
      updates.completed = completed === true ? 1 : 0;
    }
    
    await model.updateData(id, updates);
    await response.ok(res, true, "Data berhasil diupdate")
  } catch (err) {
    console.error(err);
  }
}

const deleteData = async (req, res) => {
  try {
    const id = req.params.id;
    await model.deleteData(id);
    await response.ok(res, true, "Data berhasil dihapus");

  } catch (err) {
    console.error(err);
  }
}

module.exports = {
  getData,
  getDataById,
  addData,
  updateData,
  deleteData
}
