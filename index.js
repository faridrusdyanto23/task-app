'use strict';

const express = require("express");
const app = express();
const port = 3001;
const bodyParser = require('body-parser');
const morgan = require('morgan');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));

// routing
const routes = require("./routes/index");
app.use("/", routes);

// handle error
const error = require('./middleware/notFound')
app.use(error.notFoundMiddleware);
app.use(error.handleErrors);

// starting server
app.listen(port, () => {
  console.log(`Server running on port ${port}`)
});