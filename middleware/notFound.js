'use strict';

const notFoundMiddleware = (req, res, next) => {
  const error = new Error(`Not Found - ${req.originalUrl}`);
  error.status = 404;
  next(error);
};

const handleErrors = (err, req, res, next) => {
  console.log(err);
  if (err) {
    const statusCode = err.status || 500;
    const message = err.message || 'Internal Server Error';
    const error = {
      status: statusCode,
      message: message,
    };
    res.status(statusCode).json(error);
  } else {
    next()
  }
}

module.exports = {
  notFoundMiddleware,
  handleErrors
}
