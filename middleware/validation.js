'use strict';
const response = require("../config/res");

const validation = (req, res, next) => {
  const title = req.body.title;
  if (title === '' || title === null || title === 'undefined') {
    response.ok(res, false, "title is required");
    return;
  }
  next();
}

module.exports = { validation }
