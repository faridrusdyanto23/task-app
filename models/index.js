'use strict';

const { knex } = require('../config/index');

const getAll = async () => {
  const data = await knex.select('*')
    .from('tasks')
  return data
}

const getById = async (id) => {
  const data = await knex.select('*')
    .from('tasks')
    .where('id', id)
    .first().then((row) => row)
  return data
}

const addData = async (data) => {
  await knex('tasks').insert(data)
}

const updateData = async (id, data) => {
  await knex('tasks')
    .where('id', id)
    .update(data)
}

const deleteData = async (id) => {
  await knex('tasks')
    .where('id', id)
    .del();
}

module.exports = {
  getAll,
  getById,
  addData,
  updateData,
  deleteData
}
