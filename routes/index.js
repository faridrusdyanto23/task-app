'use strict';
const express = require("express");
const router = express.Router();
const controller = require('../controllers/index');
const { validation } = require('../middleware/validation');

router.get("/tasks", controller.getData);
router.get("/tasks/:id", controller.getDataById);
router.post("/task", validation, controller.addData);
router.patch("/tasks/:id", controller.updateData);
router.delete("/tasks/:id", controller.deleteData);

module.exports = router;
